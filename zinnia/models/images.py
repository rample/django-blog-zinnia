from django.db import models
from zinnia.models.entry import Entry
from sorl.thumbnail import ImageField


class Image(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True, default='title')
    image = ImageField(upload_to='gallery', null=False)
    thumbnail = ImageField(upload_to='gallery', null=True, blank=True)

    entry = models.ForeignKey(Entry, related_name='images', null=True, blank=True)

    def get_image(self):
        return self.thumbnail or self.image

    class Meta:
        """
        Category's meta informations.
        """
        app_label = 'zinnia'